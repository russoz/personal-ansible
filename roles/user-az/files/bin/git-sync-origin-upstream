#!/bin/bash
#
# git-sync-origin-upstream
#
# local $alexei_znamensky, '<', 'russoz [AT] CPAN DOT ORG'
#
# This script will automate some steps for a developer working on a forked
# git repository:
#
#     1. Pull (and merge) changes from origin into master
#     2. Fetch changes from upstream
#     3. Merge upstream changes with master
#     4. Push the master branch (with the changes) into origin
#
# There is no dependency on github, but, as of now, the
# repository names are hard-coded as reasonably as::
#
# - upstream: the original repo from where you forked
# - origin: your forked repository
# - master: your local working copy
#
#     usage: git-sync-origin-upstream [<upstream-url>]
#
# The <upstream-url> is only necessary the first time
# git-sync-origin-upstream is run. Next time it is run, simply call
# 'git-sync-origin-upstream'.
#
# The merging procedure with the upstream repository was learned from:
#
#     http://help.github.com/forking/
#

msg() {
    echo "$@" >&2
}

die() {
    msg "$@"
    exit 1
}

find_branch() {
    br_output=$(git branch)
    for p in "$@"; do
        if echo "$br_output" | grep -q " $p\$"; then
	    echo $p
        fi
    done
}

u=upstream
o=origin

##############################################################################

url="$1"; shift
me=${0##.*/}

msg '=== Checking requisites'
git remote show | grep -q "$o" || {
    die "*** FAILED There is no tracked repository named '$o'. Aborting!!!"
}
m=$(find_branch main master devel)
[[ -z "$m" ]] && die "*** FAILED Cannot determine main branch. Aborting!!!"

git remote show | grep -q "$u" || {
    msg "!!! There is no tracked repository named '$u'."
    [ -z "$url" ] && {
        msg "*** FAILED Please specify $u repository:"
        die "***        ${me} <upstream-repository-url>"
    }
    git remote add "$u" "$url" \
    || die "*** FAILED to add remote repository $u"

    msg "=== ...done."
}
msg "=== Using existing upstream repository '$u'"

msg "=== Pulling changes from remote repository $o into $m"
git pull "$o" "$m" || die "*** FAILED to pull from $o to $m"

msg "=== Fetching from remote repo $u"
git fetch "$u" || die "*** FAILED to fetch from $u"

msg "=== Merging with $u/$m"
git merge "$u/$m" || die '!!! Merge not successful. Please verify'

msg "=== Pushing the merged tree into $o"
git push "$o" "$m" || die "*** FAILED to push changes into $o"

