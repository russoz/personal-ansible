#!/bin/bash

. ${0%/*}/lib.sh
file=main.yml

for arg in "$@"; do
  case "$arg" in
    *.yml|*.yaml) file="" ;;
    --local) opt_local="-c local -l $(hostname)"; shift
  esac
done

export PROFILE_TASKS_SORT_ORDER=none
export PROFILE_TASKS_TASK_OUTPUT_LIMIT=all
$VENV/bin/python ${VENV}/bin/ansible-playbook $opt_local $file "$@"
