Personal Ansible
================

Activation sequence:
```bash
$ ./bootstrap.sh   # only the first time
$ ./apply.sh
```
OR
```bash
$ ./apply.sh -l    # to apply it only to the local host
```
