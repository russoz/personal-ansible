#!/usr/bin/env -S bash -c "echo MUST NOT RUN THIS FILE >&2; exit 1"

BASEDIR=$(cd ${0%/*}; pwd)
VENV=${BASEDIR}/.venv
COLL=${BASEDIR}/.ansible
ROLES=${BASEDIR}/.ansible/roles

export ANSIBLE_COLLECTIONS_PATH=$COLL:$ANSIBLE_COLLECTIONS_PATH
export ANSIBLE_ROLES_PATH=$ROLES:$ANSIBLE_ROLES_PATH

msg() {
  echo "$@" >&2
}

die() {
  [[ -n "$1" ]] && msg ERROR: "$@"
  exit 1
}
