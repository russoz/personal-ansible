#!/usr/bin/env bash

APT_PACKAGES="python3-pip python3-wheel python3-dev build-essential python3-apt python3-virtualenv lastpass-cli git"
PIP_PACKAGES=requirements.txt
ANSIBLE_REQS=requirements.yml

. ${0%/*}/lib.sh

msg -n 'Install APT packages... '
sudo apt install -y $APT_PACKAGES \
|| die "Unable to install basic packages"
msg OK

msg -n "Creating virtualenv $VENV ... "
virtualenv -p python3 $VENV || die "Unable to create venv $VENV"
msg OK
PY=$VENV/bin/python

msg -n 'Install pip packages... '
$PY $VENV/bin/pip install -r $BASEDIR/$PIP_PACKAGES \
|| die "Unable to install from $BASEDIR/$PIP_PACKAGES"
msg OK

msg -n "Install collection $CG"
mkdir -p ${COLL} ${ROLES}
NETRC= $PY $VENV/bin/ansible-galaxy collection install -v -p $COLL -r $BASEDIR/$ANSIBLE_REQS \
|| die "Unable to install collections from $BASEDIR/$ANSIBLE_REQS"
NETRC= $PY $VENV/bin/ansible-galaxy role install -v -p $ROLES -r $BASEDIR/$ANSIBLE_REQS \
|| die "Unable to install roles from $BASEDIR/$ANSIBLE_REQS"
msg OK

. $VENV/bin/activate
msg -n 'Add user to group "sudo"... '
ansible -b localhost \
    -v -m user -a "name=$USER groups=sudo append=yes" >/dev/null \
|| die "Unable to add user to sudo group"
msg OK

msg -n 'Grant passwordless sudo to group "sudo"... '
ansible -b localhost \
    -v -m lineinfile \
    -a "path=/etc/sudoers
        state=present
        regexp='^%sudo'
        line='%sudo ALL=(ALL) NOPASSWD: ALL'
        validate='visudo -cf %s'" >/dev/null \
|| die "Unable to make sudo group passwordless in sudoers"
msg OK
